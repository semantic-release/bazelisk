import test from 'ava';
import Stage from '../stage.mjs';

test('`Stage` throws when `name` is not a string', t => {
	t.throws(() => new Stage(1, 'one'));
});

test('`Stage` defaults to the correct label when undefined', t => {
	const stage = new Stage('one');
	t.deepEqual(
		stage.arguments.map(a => `${a}`),
		['//release:one'],
	);
});
