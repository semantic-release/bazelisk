// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', async t => {
	const {
		verifyConditions,
		prepare,
		publish,
		notify,
	} = require('../plugin.js'); // eslint-disable-line unicorn/prefer-module
	const {temporaryDirectory} = await import('tempy');
	t.context.cfg = {stages: 1};
	t.context.ctx = {
		cwd: temporaryDirectory(),
	};
	t.is(typeof verifyConditions, 'function');
	t.is(typeof prepare, 'function');
	t.is(typeof publish, 'function');
	t.is(typeof notify, 'function');

	await t.throwsAsync(verifyConditions(t.context.cfg, t.context.ctx));
	await t.throwsAsync(prepare(t.context.cfg, t.context.ctx));
	await t.throwsAsync(publish(t.context.cfg, t.context.ctx));
	await t.throwsAsync(notify(t.context.cfg, t.context.ctx));
});
