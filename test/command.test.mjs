import test from 'ava';
import Command from '../command.mjs';

test('`Command` can be mapped', t => {
	const args = new Command(['one']);
	t.deepEqual(args.map(a => `${a}`), ['one']);
});
