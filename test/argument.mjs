import test from 'ava';
import Argument from '../argument.mjs';

test('`Argument` can be converted to a primitive', t => {
	const argument = new Argument('one');
	t.is(`${argument}`, 'one');
});
