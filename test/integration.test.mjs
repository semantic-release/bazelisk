import {EOL} from 'node:os';
import path from 'node:path';
import {cp, readFile} from 'node:fs/promises';
import {createWriteStream} from 'node:fs';
import test from 'ava';
import {restore, stub} from 'sinon';
import clearModule from 'clear-module';
import {temporaryDirectory} from 'tempy';
import {$} from 'execa';

test.before(async () => {
	const {pathname: script} = new URL(import.meta.url);
	const fixture = path.join(path.dirname(script), 'fixture');
	const $$ = $({cwd: fixture, timeout: 120 * 1000});
	await $$`bazelisk version`;
});

test.beforeEach(async t => {
	clearModule('../plugin.mjs');
	t.context.m = await import('../plugin.mjs');
	t.context.log = stub();
	t.context.tmp = temporaryDirectory();
	const dir = temporaryDirectory();

	const {pathname: script} = new URL(import.meta.url);
	const fixture = path.join(path.dirname(script), 'fixture');
	await cp(fixture, dir, {recursive: true});

	t.context.stdout = path.join(t.context.tmp, 'stdout.log');
	t.context.stderr = path.join(t.context.tmp, 'stderr.log');

	t.context.stages = ['verify', 'prepare', 'publish', 'notify'];

	t.context.cfg = {};
	t.context.ctx = {
		cwd: dir,
		env: {},
		options: {},
		stdout: createWriteStream(t.context.stdout),
		stderr: createWriteStream(t.context.stderr),
	};
	t.context.ctx.logger = {
		log: t.context.log,
		info: t.context.log,
		success: t.context.log,
		warn: t.context.log,
	};
});

test.afterEach(async t => {
	const $$ = $({cwd: t.context.ctx.cwd, timeout: 60 * 1000});

	await $$`bazelisk shutdown`;

	t.context.ctx.stdout.close();
	t.context.ctx.stderr.close();

	restore();
});

const failure = test.macro(async (t, before, after) => {
	if (before) {
		await before(t);
	}

	const [, code, pattern] = /`(.+)` error when (.+)/.exec(t.title);
	const regex = new RegExp(`${pattern[0].toUpperCase()}${pattern.slice(1)}`);

	const error = await t.throwsAsync(
		t.context.m.verifyConditions(t.context.cfg, t.context.ctx),
	);

	t.is(error.name, 'SemanticReleaseError', `${error}`);
	t.is(error.code, code, `${error}`);
	t.regex(error.message, regex, `${error}`);

	if (after) {
		await after(t);
	}
});

const success = test.macro(async (t, before, after) => {
	t.timeout(120 * 1000, 'Bazelisk did not have enough time to execute');

	if (before) {
		await before(t);
	}

	await t.context.m.verifyConditions(t.context.cfg, t.context.ctx);
	t.context.ctx.nextRelease = {version: '1.0.0'};
	await t.context.m.prepare(t.context.cfg, t.context.ctx);
	await t.context.m.publish(t.context.cfg, t.context.ctx);
	await t.context.m.notify(t.context.cfg, t.context.ctx);

	t.context.ctx.stdout.close();
	t.context.ctx.stderr.close();

	const stdout = await readFile(t.context.stdout, {encoding: 'utf8'});
	t.is(stdout, [...t.context.stages, ''].join(EOL));

	if (after) {
		await after(t);
	}
});

test('Throws `EBAZELISKCFG` error when a `stage` value must be an object', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: 1,
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].timeout` is invalid', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				arguments: '//release:prepare',
				timeout: {},
			},
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].arguments\\[\\*\\]` must be a string', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				arguments: [1],
			},
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].arguments\\[\\*\\]` failed to be templated', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				// eslint-disable-next-line no-template-curly-in-string
				arguments: ['${nope}'],
			},
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].arguments` must have a target', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				arguments: [],
			},
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].arguments` must be an array', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				arguments: 1,
			},
		},
	};
});

test('Throws `EBAZELISKCFG` error when `stages\\[\\*\\].command` must be an array', failure, async t => {
	t.context.cfg = {
		stages: {
			prepare: {
				command: 1,
				arguments: ['//release:prepare'],
			},
		},
	};
});

test('Successful operation with no configuration', success);

test('Successful operation without `prepare` target string', success, async t => {
	t.context.stages = ['verify', 'publish', 'notify'];
	t.context.cfg = {
		stages: {
			prepare: '//this/does/not:exist',
		},
	};
});

test('Successful operation with `prepare` arguments', success, async t => {
	t.context.stages = ['verify', 'prepare hello world', 'publish', 'notify'];
	t.context.cfg = {
		stages: {
			prepare: ['//release:prepare', 'hello', 'world'],
		},
	};
});

test('Successful operation with `prepare` build', success, async t => {
	t.context.stages = ['verify', 'publish', 'notify'];
	t.context.cfg = {
		stages: {
			prepare: {
				command: 'build',
				arguments: '//release:prepare',
			},
		},
	};
});

for (const timeout of [120_000, '1m', 'infinity', '∞']) {
	test(`Successful operation with \`${timeout}\` timeout`, success, async t => {
		t.context.stages = ['verify', 'publish', 'notify'];
		t.context.cfg = {
			stages: {
				prepare: {
					command: 'build',
					timeout,
					arguments: '//release:prepare',
				},
			},
		};
	});
}

test('Successful operation without `prepare` target array', success, async t => {
	t.context.stages = ['verify', 'publish', 'notify'];
	t.context.cfg = {
		stages: {
			prepare: ['//this/does/not:exist'],
		},
	};
});
