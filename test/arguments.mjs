import test from 'ava';
import Arguments from '../arguments.mjs';

test('`Arguments` can be mapped', t => {
	const args = new Arguments(['one']);
	t.deepEqual(args.map(a => `${a}`), ['one']);
});
