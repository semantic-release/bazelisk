visibility("//...")

DOC = ""

ATTRS = {
    "_windows": attr.label(
        providers = [platform_common.ConstraintValueInfo],
        default = "@platforms//os:windows",
    ),
}

def implementation(ctx):
    constraint = ctx.attr._windows[platform_common.ConstraintValueInfo]
    windows = ctx.target_platform_has_constraint(constraint)
    script = "bat" if windows else "sh"
    echo = "@echo." if windows else 'echo '
    args = "%*" if windows else '"${@}"'
    executable = ctx.actions.declare_file("{}.{}".format(ctx.label.name, script))
    content = "{}{} {}".format(echo, ctx.label.name, args)
    ctx.actions.write(executable, content, is_executable = True)

    return DefaultInfo(
        executable = executable,
        files = depset([executable]),
        runfiles = ctx.runfiles([executable]),
    )

echo = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    executable = True,
)
