import SemanticReleaseError from '@semantic-release/error';
import Argument from './argument.mjs';

export default class Command {
	#value;

	constructor(value) {
		if (value === undefined) {
			value = ['run'];
		}

		if (typeof value === 'string') {
			value = [value];
		}

		if (!Array.isArray(value)) {
			throw new SemanticReleaseError(
				'`stages[*].command` must be an array',
				'EBAZELISKCFG',
				`${value} (${typeof value})`,
			);
		}

		this.#value = value;
	}

	[Symbol.iterator] = function * () {
		for (const value of this.#value) {
			yield new Argument(value);
		}
	};

	get #array() {
		return Array.from(this);
	}

	map(...args) {
		return this.#array.map(...args);
	}

	async render(ctx) {
		const promises = this.#array.map(a => a.render(ctx));
		const values = await Promise.all(promises);
		return values.flat();
	}
}
