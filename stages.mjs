import SemanticReleaseError from '@semantic-release/error';
import Stage from './stage.mjs';

export default class Stages {
	#value;

	constructor(value) {
		if (value === undefined) {
			value = {};
		}

		if (typeof value !== 'object' || Array.isArray(value)) {
			throw new SemanticReleaseError(
				'`stages` must be an object',
				'EBAZELISKCFG',
				`${value} (${typeof value})`,
			);
		}

		this.#value = {
			verify: '//release:verify',
			prepare: '//release:prepare',
			publish: '//release:publish',
			notify: '//release:notify',
			...value,
		};
	}

	[Symbol.iterator] = function * () {
		for (const [name, value] of Object.entries(this.#value)) {
			yield new Stage(name, value);
		}
	};

	get #array() {
		return Array.from(this);
	}

	map(...args) {
		return this.#array.map(...args);
	}

	get #map() {
		return new Map(this.map(stage => [stage.name, stage]));
	}

	get verify() {
		return this.#map.get('verify');
	}

	get prepare() {
		return this.#map.get('prepare');
	}

	get publish() {
		return this.#map.get('publish');
	}

	get notify() {
		return this.#map.get('notify');
	}
}
