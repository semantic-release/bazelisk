import SemanticReleaseError from '@semantic-release/error';
import {template} from 'lodash-es';

export default class Argument {
	#value;

	constructor(value) {
		if (typeof value !== 'string') {
			throw new SemanticReleaseError(
				'`stages[*].arguments[*]` must be a string',
				'EBAZELISKCFG',
				`${value} (${typeof value})`,
			);
		}

		this.#value = value;
	}

	get value() {
		return this.#value;
	}

	[Symbol.toPrimitive]() {
		return this.value;
	}

	get template() {
		return template(this.value);
	}

	async #rendered(ctx) {
		try {
			return this.template(ctx);
		} catch (error) {
			throw new SemanticReleaseError(
				'`stages[*].arguments[*]` failed to be templated',
				'EBAZELISKCFG',
				`${error}`,
			);
		}
	}

	async render(ctx) {
		const rendered = await this.#rendered(ctx);
		return `${rendered}`;
	}
}
