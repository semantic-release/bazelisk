import SemanticReleaseError from '@semantic-release/error';
import Argument from './argument.mjs';

export default class Arguments {
	#value;

	constructor(value) {
		if (typeof value === 'string') {
			value = [value];
		}

		if (!Array.isArray(value)) {
			throw new SemanticReleaseError(
				'`stages[*].arguments` must be an array',
				'EBAZELISKCFG',
				`${value} (${typeof value})`,
			);
		}

		if (value.length === 0) {
			throw new SemanticReleaseError(
				'`stages[*].arguments` must have a target',
				'EBAZELISKCFG',
				`${value}`,
			);
		}

		this.#value = value;
	}

	[Symbol.iterator] = function * () {
		for (const value of this.#value) {
			yield new Argument(value);
		}
	};

	get #array() {
		return Array.from(this);
	}

	map(...args) {
		return this.#array.map(...args);
	}

	async render(ctx) {
		const promises = this.#array.map(a => a.render(ctx));
		const values = await Promise.all(promises);
		return values.flat();
	}
}
