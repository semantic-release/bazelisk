import SemanticReleaseError from '@semantic-release/error';
import ms from 'ms';
import Arguments from './arguments.mjs';
import Command from './command.mjs';

export default class Stage {
	#name;
	#arguments;
	#command;
	#timeout;
	#query;

	constructor(name, value) {
		if (typeof name !== 'string') {
			throw new TypeError(
				`A \`stage\` name must be a string: ${name} (${typeof name})`,
			);
		}

		this.#name = name;

		if (value === undefined) {
			value = {arguments: [`//release:${name}`]};
		}

		if (typeof value === 'string') {
			value = {arguments: [value]};
		}

		if (Array.isArray(value)) {
			value = {arguments: value};
		}

		if (typeof value !== 'object' || Array.isArray(value)) {
			throw new SemanticReleaseError(
				'A `stage` value must be an object',
				'EBAZELISKCFG',
				`${value} (${typeof value})`,
			);
		}

		const {arguments: args, command, timeout = '∞', query = true} = value;
		this.#arguments = new Arguments(args);
		this.#command = new Command(command);
		this.#query = query;

		if (typeof timeout === 'number') {
			this.#timeout = timeout;
		} else if (['infinity', '∞'].includes(timeout)) {
			this.#timeout = undefined;
		} else if (typeof timeout === 'string') {
			this.#timeout = ms(timeout);
		} else {
			throw new SemanticReleaseError(
				'A `stages[*].timeout` is invalid.',
				'EBAZELISKCFG',
				`${timeout} (${typeof timeout})`,
			);
		}
	}

	get name() {
		return this.#name;
	}

	get arguments() {
		return this.#arguments;
	}

	get command() {
		return this.#command;
	}

	get timeout() {
		return this.#timeout;
	}

	get query() {
		return this.#query;
	}

	async render(ctx) {
		const [cmd, args] = await Promise.all([
			this.command.render(ctx),
			this.arguments.render(ctx),
		]);
		return [...cmd, '--ui_event_filters', '-info', '--', ...args];
	}
}
