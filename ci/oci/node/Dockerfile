ARG VERSION=18
FROM node:${VERSION}-bookworm-slim

# Prevent configuration of packages
ARG DEBIAN_FRONTEND=noninteractive

# Optimise for APT package cache
# `type=cache` folders are mounted that keep our layers clean when using `apt install`
# `docker-clean` script performs package clean up in a post invoke hook, remove that
# `keep-cache` tells APT to store packages in the mounted cache folder
RUN rm -f /etc/apt/apt.conf.d/docker-clean \
 && echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

# Install tools
RUN \
 --mount=type=cache,target=/var/cache/apt,sharing=locked \
 --mount=type=cache,target=/var/lib/apt,sharing=locked \
    apt update \
 && apt --yes --no-install-recommends install \
      git-core \
      ca-certificates

# Create unprivileged user
RUN useradd -ms /bin/bash -u 1337 ci
USER ci
WORKDIR /home/ci

# Allow global installation of NPM packages without root access
ENV NPM_PACKAGES /home/ci/.local
RUN mkdir -p "$NPM_PACKAGES" \
 && npm config set -- prefix "${NPM_PACKAGES}"
ENV NODE_PATH "$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
ENV PATH "$NPM_PACKAGES/bin:$PATH"
ENV MANPATH "$NPM_PACKAGES/share/man:$(manpath)"

# Set up GitLab NPM registry access
ARG CI_API_V4_URL
RUN npm --version \
 && npm config set always-auth true; \
    npm config set -- "${CI_API_V4_URL#http*:}/packages/npm/:_authToken" '${CI_JOB_TOKEN}' \
 && rm -rf ~/.npm/_logs
