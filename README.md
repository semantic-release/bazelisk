# Bazelisk

A `semantic-release` plugin to run [Bazelisk][bazelisk] targets for each
`semantic-release` stage.

| Step             | Description                            |
| ---------------- | -------------------------------------- |
| verifyConditions | Runs `//release:verify`, if available  |
| prepare          | Runs `//release:prepare`, if available |
| publish          | Runs `//release:publish`, if available |
| notify           | Runs `//release:notify`, if available  |

## Getting Started

```sh
npm config --location project set registry https://gitlab.arm.com/api/v4/groups/semantic-release/-/packages/npm/
npm install --save-dev @semantic-release/bazelisk
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - "@semantic-release/bazelisk"
```

## Usage

Add a `release/BUILD.bazel` file with the stage targets:

```py
alias(
    name = "prepare",
    actual = "//some:target",
)
```

The `//some:target` would be ran as part of the `prepare` stage.

Custom rules can be written to customise what Bazelisk performs in that stage.

## Configuration

The command and arguments to `bazelisk <command> -- <arguments>` can be
controlled for each stage:

```yaml
plugins:
  - path: "@semantic-release/yq"
    stages:
      verify: "//something:verify"
      prepare:
        - "//something:else"
      publish:
        query: false
        timeout: "60s"
        command:
          - "run"
          - "--stamp"
          - "--embed_label=${nextRelease.version}"
        arguments: "//some:target"
      verify:
        arguments:
        - "//something:upload"
        - "--url=https://somewhere.com/upload/endpoint"
```

## `stages[*].command`

Can be a string as a convenience for a single element array.

Otherwise, an array of arguments to use for the command portion of
`bazelisk <command> -- <arguments>`.

Defaults to `run`.

The command can consume [substitutions] in a [lodash template].

## `stages[*].arguments`

Can be a string as a convenience for a single element array.

Otherwise, an array of arguments to use for the arguments portion of
`bazelisk <command> -- <arguments>`.

Defaults to `//release:<stage-name>`.

The arguments can consume [substitutions] in a [lodash template].

# `stages[*].query`

Determines if `bazelisk query -- <target>` is ran to detect if the target
exists. If the target is not available, the stage does nothing.

The default is `true`.

Setting it to `false` will run the command and fail if the target does not
exist.

# `stages[*].timeout`

Sets the timeout for `bazelisk` executions.

Can be anything that [ms] understands (such as `"60s"`, `"1m"`), the number of
milliseconds, `"infinity"` or `"∞"`.

Defaults to an unlimited timeout.

[bazelisk]: https://github.com/bazelbuild/bazelisk
[lodash template]: https://docs-lodash.com/v4/template/
[substitutions]:
  https://semantic-release.gitbook.io/semantic-release/developer-guide/js-api#nextrelease
[ms]: https://www.npmjs.com/package/ms
