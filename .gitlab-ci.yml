stages:
  - install
  - lint
  - build
  - test
  - deploy

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED == "true"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_FORCE == "true"

oci:
  needs: []
  stage: .pre
  image: docker:24.0.2
  services:
    - docker:24.0.2-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    IMAGE: "$CI_REGISTRY_IMAGE/$NAME"
    ROOT: "ci/oci"
    CONTEXT: "$ROOT/$NAME"
    REGISTRY: "$CI_REGISTRY"
    USERNAME: "$CI_REGISTRY_USER"
    PASSWORD: "$CI_REGISTRY_PASSWORD"
  parallel:
    matrix:
      - NAME:
          - node
          - reuse
  script:
    - docker info
    - echo "$PASSWORD" | docker login --username "$USERNAME" --password-stdin
      "$REGISTRY"
    - |
      docker buildx build \
        --build-arg BUILDKIT_INLINE_CACHE=1 \
        --build-arg CI_REGISTRY_IMAGE \
        --build-arg CI_COMMIT_SHA \
        --build-arg CI_API_V4_URL \
        --cache-from=type=registry,ref=$IMAGE:$CI_COMMIT_BEFORE_SHA \
        --cache-from=type=registry,ref=$IMAGE:$CI_MERGE_REQUEST_DIFF_BASE_SHA \
        --tag=$IMAGE:$CI_COMMIT_SHA \
        --push \
        "$CONTEXT"

reuse:
  needs:
    - oci
  image: $CI_REGISTRY_IMAGE/reuse:$CI_COMMIT_SHA
  stage: lint
  script:
    - reuse lint

.node:
  image: $CI_REGISTRY_IMAGE/node:$CI_COMMIT_SHA
  variables:
    BAZELISK_HOME: "${CI_PROJECT_DIR}/.cache/bazelisk"
  cache:
    policy: pull
    key:
      prefix: nodejs
      files:
        - package-lock.json
    paths:
      - node_modules
      - .cache

install:
  needs:
    - oci
  extends: .node
  stage: install
  cache:
    policy: pull-push
  script:
    - npm ci --cache .cache/npm --prefer-offline
    - (cd test/fixture; npx bazelisk version)

lint:
  needs:
    - install
  extends: .node
  stage: lint
  script:
    - npm run lint

test:
  needs:
    - install
  extends: .node
  stage: test
  script:
    - |
      set -euo pipefail
      npx c8 --reporter cobertura ava --tap | tee /dev/fd/2 | npx tap-junit -s @$CI_PROJECT_PATH -o .
  artifacts:
    reports:
      junit: tap.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml

semantic-release:
  needs:
    - lint
    - test
  extends: .node
  stage: deploy
  variables:
    NPM_TOKEN: "${CI_JOB_TOKEN}"
  before_script:
    - npm config set -- "@${CI_PROJECT_ROOT_NAMESPACE}:registry"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
  script:
    - npx semantic-release
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true"
