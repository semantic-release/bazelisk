import {EOL} from 'node:os';
import debug from 'debug';
import {$} from 'execa';
import Stages from './stages.mjs';

debug('semantic-release:bazelisk');

async function run(context, stage) {
	const {logger, stdout, stderr, cwd, ...ctx} = context;
	const $$ = $({timeout: stage.timeout, cwd});

	const args = await stage.arguments.render(ctx);
	const [target] = args;

	if (stage.query) {
		const filter = `filter(${target}, //...)`;
		const {stdout: data} = await $$`bazelisk query -- ${filter}`;

		const lines = data.split(EOL);
		if (!lines.includes(target)) {
			logger.warn('Skipping `%s` target', stage.name);
			return;
		}
	}

	const rendered = await stage.render(ctx);

	logger.info('Running: %s', rendered);
	await $$({stdout, stderr})`bazelisk ${rendered}`;
}

export async function verifyConditions(pluginConfig, context) {
	const {stages: value} = pluginConfig;
	const {logger, ...rest} = context;

	const stages = new Stages(value);

	const ctx = {
		nextRelease: {
			type: 'patch',
			version: '0.0.0',
			gitHead: '0123456789abcedf0123456789abcdef12345678',
			gitTag: 'v0.0.0',
			notes: 'placeholder',
		},
		...rest,
	};

	const checks = stages.map(async stage => {
		const rendered = await stage.render(ctx);
		logger.success('Validated `%s`: %s', stage.name, rendered);
	});

	await Promise.all(checks);
	logger.success('Validated `stages` configuration');

	await run(context, stages.verify);
}

async function hook(name, pluginConfig, context) {
	const {stages: value} = pluginConfig;

	const stages = new Stages(value);

	const stage = stages[name];
	/* c8 ignore next 3 */
	if (stage === undefined) {
		throw new Error(`Invalid stage name: ${name}`);
	}

	await run(context, stage);
}

export const prepare = async (...args) => hook('prepare', ...args);
export const publish = async (...args) => hook('publish', ...args);
export const notify = async (...args) => hook('notify', ...args);
